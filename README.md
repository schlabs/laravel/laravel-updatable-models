
# laravel-updatable-models
  

## Description

Updatable fields for Laravel models
  

## Depencencies

* none
  

## Installation
  

**In your composer.json**

Add the following code to the "repositories" key:

```json
"repositories": [
	{
		"type": "vcs",
		"url": "https://gitlab.com/schlabs/laravel/laravel-updatable-models"
	}
]
```

Add this line to your require dependecies:

```json
"schlabs/laravel-updatable-models": "1.0.*"
```
 
**In your project root run:**
```sh
composer update
```

## Usage

1. Include the trait "SchLabs\LaravelUpdatableModels\Traits\IsUpdatable" in your Model class.
2. Add the $updatable property to your model, with the allowed fields to update.


