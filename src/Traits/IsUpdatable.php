<?php
namespace SchLabs\LaravelUpdatableModels\Traits;

trait IsUpdatable
{
    /**
     * Update a model, only allow changes on the fields that are in the "updatable" attribute
     *
     * @param array $attributes
     * @param array $options
     * @return false
     */
    public function update(array $attributes = [], array $options = []): bool
    {
        $canUpdate = [];

        if(!empty($this->getUpdatable())){
            $updatable = array_flip($this->getUpdatable());
            $bases = $this->getBases($attributes);
            collect($bases)->intersectByKeys($updatable)
                ->each(function($item) use (&$canUpdate){
                    $canUpdate[$item['key']] = $item['value'];
                });
        }

        return parent::update($attributes, $options);
    }

    /**
     * @return array
     */
    public function getUpdatable(): array
    {
        return property_exists($this, 'updatable') ? $this->updatable: [];
    }

    public function getBases(array $attributes): array
    {
        return
            collect($attributes)->mapWithKeys(function($value, $key){
                $pieces = explode('->', $key);
                return [
                    $pieces[0] => [
                        'key' => $key,
                        'value' => $value
                    ]
                ];
            })
            ->all();
    }
}
